/*****************************************************************************
@Name:  CaseTrigger     
@=========================================================================
@Purpose: Case Trigger evaluating business logic for productivity                                                                                                   
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
 ******************************************************************************/ 
trigger CaseTrigger on Case (after insert, after update, before insert, before update) {
        if(Trigger.isInsert){ // on Insert
           if(Trigger.isBefore){ //on before insert
               TriggerProductivityHandler.onbeforeInsert(Trigger.new,Trigger.old,Trigger.newMap,Trigger.oldMap);
           }
           if(Trigger.isAfter){ //  on after insert
               TriggerProductivityHandler.onAfterInsert(Trigger.new,Trigger.old,Trigger.newMap,Trigger.oldMap);
           }
           
        } 
       
        else if(Trigger.isUpdate){    // on Update
           if(Trigger.isBefore){  // on before update
               TriggerProductivityHandler.onBeforeUpdate(Trigger.new,Trigger.old,Trigger.newMap,Trigger.oldMap);
           }
           if(Trigger.isAfter){  // on after update
               TriggerProductivityHandler.onAfterUpdate(Trigger.new,Trigger.old,Trigger.oldMap,Trigger.newMap);
           }
        }
}