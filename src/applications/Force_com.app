<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Start Here</description>
    <label>Force.com</label>
    <tab>Productivity_Configuration__c</tab>
    <tab>Productivity__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Productivity_Field_Configuration__c</tab>
</CustomApplication>
