/*****************************************************************************
@Name:  TriggerProductivityBusinessLogic     
@=========================================================================
@Purpose: Business Logic for productivity                                                                                              
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
 ******************************************************************************/ 
global class TriggerProductivityBusinessLogic implements TriggerProductivityInterface{
	
	/*****************************************************************************
	@Name:  createProductivity     
	@=========================================================================
	@Purpose: Insert productivity object                                                                                            
	@=========================================================================
	@History                                                            
	@---------                                                            
	@VERSION AUTHOR                            DATE                DETAIL                                 
	@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
	 ******************************************************************************/ 
    global void createProductivity(Map<ID,Sobject> insertProductivityRecord, Set<String> fetchFieldsLst){ 
         List<EPTE__Productivity__c> insertProductivitylst = new List<EPTE__Productivity__c>();
         BusinessHours bhsDefault = ProductivityDAL.fetchBusinessHours();
         for(Sobject sobj : insertProductivityRecord.values()){
         	EPTE__Productivity__c epteProductivity = new EPTE__Productivity__c();
         	String fetchObname = ProductivityUtil.fetchObjectname(sobj);
         	if(fetchFieldsLst.contains(fetchObname)){
         		epteProductivity.EPTE__sObject_Name__c = fetchObname;
         	}
         	epteProductivity.EPTE__EmployeeName__c = String.valueof(sobj.get('OwnerID')); 
         	epteProductivity.EPTE__Start_Time__c = System.now();
         	epteProductivity.EPTE__sObject_Record_Id__c = sobj.Id;
         	epteProductivity.EPTE__Business_Hours__c = bhsDefault.ID;
         	insertProductivitylst.add(epteProductivity);
         }
         try{
         	insert insertProductivitylst;  
         }catch(Exception de){
            System.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'+de);
         }
    }
    /*****************************************************************************
	@Name:  createNUpdateProductivity     
	@=========================================================================
	@Purpose: Insert and update productivity object(Close time)                                                                                            
	@=========================================================================
	@History                                                            
	@---------                                                            
	@VERSION AUTHOR                            DATE                DETAIL                                 
	@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
	 ******************************************************************************/ 
    global void createNUpdateProductivity(Map<ID,Sobject> insertProductivityRecord, Set<String> fetchFieldsLst){ 
         List<EPTE__Productivity__c> insertProductivitylst = new List<EPTE__Productivity__c>();
          BusinessHours bhsDefault = ProductivityDAL.fetchBusinessHours();
         for(Sobject sobj : insertProductivityRecord.values()){
         	EPTE__Productivity__c epteProductivity = new EPTE__Productivity__c();
         	String fetchObname = ProductivityUtil.fetchObjectname(sobj);
         	if(fetchFieldsLst.contains(fetchObname)){
         		epteProductivity.EPTE__sObject_Name__c = fetchObname;
         	}
         	epteProductivity.EPTE__EmployeeName__c = String.valueof(sobj.get('OwnerID')); 
         	epteProductivity.EPTE__Start_Time__c = System.now();
         	epteProductivity.EPTE__sObject_Record_Id__c = sobj.Id;
         	epteProductivity.EPTE__Business_Hours__c = bhsDefault.ID;
         	insertProductivitylst.add(epteProductivity);
         }
         insert insertProductivitylst;  
         
         List<EPTE__Productivity__c>  fetchOldProductivityRecord = ProductivityDAL.fetchToCloseDate(insertProductivityRecord.keyset(),insertProductivitylst); 
         List<EPTE__Productivity__c> updateProductivity = new List<EPTE__Productivity__c>();
         if(fetchOldProductivityRecord <> null && fetchOldProductivityRecord.size()>0){
         	for(EPTE__Productivity__c iterateProduct:fetchOldProductivityRecord){
         		iterateProduct.EPTE__End_Time__c = System.now();
         		iterateProduct.EPTE__Productivity_in_Business_Hours__c = BusinessHours.diff(iterateProduct.EPTE__Business_Hours__c,iterateProduct.EPTE__Start_Time__c,iterateProduct.EPTE__End_Time__c)/1000;
         		
         		updateProductivity.add(iterateProduct);
         	}
         } 
         try{
         	update updateProductivity;
         }catch(Exception e){
         	
         }
    }
    /*****************************************************************************
	@Name:  closeProductivity     
	@=========================================================================
	@Purpose: Closing productivity object(Close time)                                                                                            
	@=========================================================================
	@History                                                            
	@---------                                                            
	@VERSION AUTHOR                            DATE                DETAIL                                 
	@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
	 ******************************************************************************/ 
    global void closeProductivity(Map<ID,Sobject> insertProductivityRecord, Set<String> fetchFieldsLst){
    	List<EPTE__Productivity__c>  fetchOldProductivityRecord = ProductivityDAL.fetchToCloseProductivity(insertProductivityRecord.keyset());
        List<EPTE__Productivity__c> updateProductivity = new List<EPTE__Productivity__c>();
        if(fetchOldProductivityRecord <> null && fetchOldProductivityRecord.size()>0){
         	for(EPTE__Productivity__c iterateProduct:fetchOldProductivityRecord){
         		iterateProduct.EPTE__End_Time__c = System.now();
         		iterateProduct.EPTE__Productivity_in_Business_Hours__c = BusinessHours.diff(iterateProduct.EPTE__Business_Hours__c,iterateProduct.EPTE__Start_Time__c,iterateProduct.EPTE__End_Time__c)/1000;
         		updateProductivity.add(iterateProduct);
         	}
       }
       try{
     	update updateProductivity;
       }catch(Exception e){
       }
    }
}