/*****************************************************************************
@Name:  TriggerProductivityInterface     
@=========================================================================
@Purpose: Declaring the functions used for productivity                                                                                                 
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
 ******************************************************************************/ 
global interface TriggerProductivityInterface {
    
       void createProductivity(Map<ID,Sobject> insertProductivityRecord, Set<String> fetchFieldsLst);
       void createNUpdateProductivity(Map<ID,Sobject> insertProductivityRecord, Set<String> fetchFieldsLst);
       void closeProductivity(Map<ID,Sobject> insertProductivityRecord, Set<String> fetchFieldsLst);
}