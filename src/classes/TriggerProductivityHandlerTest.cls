/*****************************************************************************
@Name:  TriggerProductivityHandlerTest     
@=========================================================================
@Purpose: Test class for Productivity Trigger                                                                                           
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-03-2016         INITIAL DEVELOPMENT              
******************************************************************************/ 
@isTest
global class TriggerProductivityHandlerTest {

    static testMethod void CaseCreationTest() {
          BusinessHours bhs=[SELECT Id,IsDefault FROM BusinessHours where IsDefault = true];
           
          User userNames = UtilityTest.getdummyUser();
          insert userNames;
          System.assertEquals(usernames.Alias ,'Cloudg');
          EPTE__Productivity_Configuration__c productivityConfig = new EPTE__Productivity_Configuration__c();
          
          productivityConfig .EPTE__Business_Hours__c = bhs.ID;
          productivityConfig .EPTE__Sobject_Type__c = 'Case';
          insert productivityConfig ;
          
          EPTE__Productivity_Field_Configuration__c productivityentry = new EPTE__Productivity_Field_Configuration__c();
          productivityentry.EPTE__FieldAPIName__c = 'Status';
          productivityentry.EPTE__Field_Name__c = 'Status';
          productivityentry.EPTE__Rule_Type__c = 'Entry';
          productivityentry.EPTE__Value__c = 'Assigned';
          productivityentry.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityentry;
          
          EPTE__Productivity_Field_Configuration__c productivityExit = new EPTE__Productivity_Field_Configuration__c();
          productivityExit.EPTE__FieldAPIName__c = 'Origin';
          productivityExit.EPTE__Field_Name__c = 'Origin';
          productivityExit.EPTE__Rule_Type__c = 'Exit';
          productivityExit.EPTE__Value__c = 'Email';
          productivityExit.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityExit;
          
          List<Case> Casesobject = new list<Case>();
          
          for(Integer ints=0;ints<=5;ints++){
              Case cses= new Case();
              cses=UtilityTest.getdummyCase(ints);
              Casesobject.add(cses);
          }
          
          test.starttest();
          insert Casesobject;
          test.stoptest();
          
          List<Case> CasesobjectUpdate = new list<Case>();
          for(Case objs:Casesobject){
             objs.OwnerId = userNames.ID;
             CasesobjectUpdate.add(objs);
          }
          update CasesobjectUpdate;
          
          List<Case> CasesobjectUpdateExit = new list<Case>();
          
          for(Case objs:CasesobjectUpdate){
             objs.Origin = 'Email';
             CasesobjectUpdateExit.add(objs);
          }

          update CasesobjectUpdateExit;
    }
        
    static testMethod void LeadCreationTest() {
          BusinessHours bhs=[SELECT Id,IsDefault FROM BusinessHours where IsDefault = true];
          
          User userNames = UtilityTest.getdummyUser();
          insert userNames;
          System.assertEquals(usernames.Alias ,'Cloudg');
          EPTE__Productivity_Configuration__c productivityConfig = new EPTE__Productivity_Configuration__c();
          
          productivityConfig .EPTE__Business_Hours__c = bhs.ID;
          productivityConfig .EPTE__Sobject_Type__c = 'Lead';
          insert productivityConfig ;
          
          EPTE__Productivity_Field_Configuration__c productivityentry = new EPTE__Productivity_Field_Configuration__c();
          productivityentry.EPTE__FieldAPIName__c = 'Company';
          productivityentry.EPTE__Field_Name__c = 'Company';
          productivityentry.EPTE__Rule_Type__c = 'Entry';
          productivityentry.EPTE__Value__c = 'ET';
          productivityentry.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityentry;
          
          EPTE__Productivity_Field_Configuration__c productivityExit = new EPTE__Productivity_Field_Configuration__c();
          productivityExit.EPTE__FieldAPIName__c = 'Status';
          productivityExit.EPTE__Field_Name__c = 'Status';
          productivityExit.EPTE__Rule_Type__c = 'Exit';
          productivityExit.EPTE__Value__c = 'Closed - Converted ';
          productivityExit.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityExit;
          
          List<Lead> Leadsobject = new list<Lead>();
          
          for(Integer ints=0;ints<=5;ints++){
              Lead lds= new Lead();
              lds=UtilityTest.getdummyLead(ints);
              Leadsobject .add(lds);
          }
          test.starttest();
          insert Leadsobject;
          test.stoptest();
          List<Lead> LeadsobjectLst = new list<Lead>();
          for(Lead lds:Leadsobject){
              lds.OwnerId = userNames.ID;
              LeadsobjectLst.add(lds);
          }
          
          update LeadsobjectLst;
          List<Lead> LeadsobjectLstExit = new list<Lead>();
          for(Lead lds: LeadsobjectLst){
              lds.Status = 'Closed - Converted';
              LeadsobjectLstExit .add(lds);
          }
          
          update LeadsobjectLstExit ;
          
    }
    static testMethod void AccountCreationTest() {
          BusinessHours bhs=[SELECT Id,IsDefault FROM BusinessHours where IsDefault = true];
          User userNames = UtilityTest.getdummyUser();
          insert userNames;
          System.assertEquals(usernames.Alias ,'Cloudg');
          
          EPTE__Productivity_Configuration__c productivityConfig = new EPTE__Productivity_Configuration__c();
          
          productivityConfig .EPTE__Business_Hours__c = bhs.ID;
          productivityConfig .EPTE__Sobject_Type__c = 'Case';
          insert productivityConfig ;
          
          EPTE__Productivity_Field_Configuration__c productivityentry = new EPTE__Productivity_Field_Configuration__c();
          productivityentry.EPTE__FieldAPIName__c = 'Type';
          productivityentry.EPTE__Field_Name__c = 'Type';
          productivityentry.EPTE__Rule_Type__c = 'Entry';
          productivityentry.EPTE__Value__c = 'Prospect';
          productivityentry.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityentry;
          
          EPTE__Productivity_Field_Configuration__c productivityExit = new EPTE__Productivity_Field_Configuration__c();
          productivityentry.EPTE__FieldAPIName__c = 'Type';
          productivityentry.EPTE__Field_Name__c = 'Type';
          productivityentry.EPTE__Rule_Type__c = 'Entry';
          productivityentry.EPTE__Value__c = 'Technology Partner';
          productivityExit.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityExit;
          
          List<Account> Accountsobject = new list<Account>();
          
          for(Integer ints=0;ints<=5;ints++){
              Account  accInsert = new Account ();
              accInsert =UtilityTest.getdummyAccount(ints);
              Accountsobject.add(accInsert );
          }
          test.starttest();
          insert Accountsobject;
          test.stoptest();
          List<Account> AccountsobjectUpdate = new list<Account>();
          for(Account aclst : Accountsobject){
               aclst.OwnerId = userNames.ID;
               AccountsobjectUpdate.add(aclst );
          }
          update AccountsobjectUpdate;
          
          List<Account> AccountsobjectExit = new list<Account>();
          
          for(Account accInsert: Accountsobject){
               accInsert.Type= 'Technology Partner';
               AccountsobjectExit.add(accInsert);
          }
          update AccountsobjectExit;
    }
    
    static testMethod void TaskCreationTest() {
    
          BusinessHours bhs=[SELECT Id,IsDefault FROM BusinessHours where IsDefault = true];
          User userNames = UtilityTest.getdummyUser();
          insert userNames;
          System.assertEquals(usernames.Alias ,'Cloudg');
          
          EPTE__Productivity_Configuration__c productivityConfig = new EPTE__Productivity_Configuration__c();
          
          productivityConfig .EPTE__Business_Hours__c = bhs.ID;
          productivityConfig .EPTE__Sobject_Type__c = 'Case';
          insert productivityConfig ;
          
          EPTE__Productivity_Field_Configuration__c productivityentry = new EPTE__Productivity_Field_Configuration__c();
          productivityentry.EPTE__FieldAPIName__c = 'Status';
          productivityentry.EPTE__Field_Name__c = 'Status';
          productivityentry.EPTE__Rule_Type__c = 'Entry';
          productivityentry.EPTE__Value__c = 'In Progress';
          productivityentry.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityentry;
          
          EPTE__Productivity_Field_Configuration__c productivityExit = new EPTE__Productivity_Field_Configuration__c();
          productivityExit.EPTE__FieldAPIName__c = 'Status';
          productivityExit.EPTE__Field_Name__c = 'Status';
          productivityExit.EPTE__Rule_Type__c = 'Exit';
          productivityExit.EPTE__Value__c = 'Completed';
          productivityExit.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityExit;
          
          List<Task> taskInsert = new list<Task>();
          for(Integer ints=0;ints<=5;ints++){
              Task tskInsert = new Task();
              tskInsert =UtilityTest.getdummyTask(ints);
              taskInsert.add(tskInsert );
          }
          
          test.starttest();
          insert taskInsert;
          test.stoptest();
          
          List<Task> taskInsertUpdate = new list<Task>();
          for(Task tsk :taskInsert){
             tsk.OwnerId = userNames.ID;
             taskInsertUpdate.add(tsk);
          }
          
          update taskInsertUpdate;
          List<Task> taskInsertUpdateExit = new list<Task>();
          
          for(Task tsk :taskInsertUpdate){
             tsk.Status = 'Completed';
             taskInsertUpdateExit .add(tsk);
          }
          update taskInsertUpdateExit;
    }
    
    static testMethod void OpportunityCreationTest() {
          BusinessHours bhs=[SELECT Id,IsDefault FROM BusinessHours where IsDefault = true];
          User userNames = UtilityTest.getdummyUser();
          insert userNames;
          System.assertEquals(usernames.Alias ,'Cloudg');
          
          EPTE__Productivity_Configuration__c productivityConfig = new EPTE__Productivity_Configuration__c();
          
          productivityConfig .EPTE__Business_Hours__c = bhs.ID;
          productivityConfig .EPTE__Sobject_Type__c = 'Case';
          insert productivityConfig ;
          
          EPTE__Productivity_Field_Configuration__c productivityentry = new EPTE__Productivity_Field_Configuration__c();
          productivityentry.EPTE__FieldAPIName__c = 'Stage';
          productivityentry.EPTE__Field_Name__c = 'Stage';
          productivityentry.EPTE__Rule_Type__c = 'Entry';
          productivityentry.EPTE__Value__c = 'Prospecting';
          productivityentry.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityentry;
          
          EPTE__Productivity_Field_Configuration__c productivityExit = new EPTE__Productivity_Field_Configuration__c();
          productivityExit.EPTE__FieldAPIName__c = 'Stage';
          productivityExit.EPTE__Field_Name__c = 'Stage';
          productivityExit.EPTE__Rule_Type__c = 'Exit';
          productivityExit.EPTE__Value__c = 'Closed Won';
          productivityExit.EPTE__Productivity_Configuration__c = productivityConfig.ID;
          insert productivityExit;
          
          List<Opportunity> oppList = new list<Opportunity>();
          
          for(Integer ints=0;ints<=5;ints++){
              Opportunity oppInsert = new Opportunity();
              oppInsert =UtilityTest.getdummyOpportunity(ints);
              oppList.add(oppInsert);
          }
          
          test.starttest();
          insert oppList;
          test.stoptest();
         
          List<Opportunity> oppListUpdate = new list<Opportunity>();
          
          for(Opportunity opp:oppList){
             opp.OwnerId = userNames.ID;
             oppListUpdate.add(opp);
          }
          
          update oppListUpdate;
          
          List<Opportunity> oppListUpdateExit = new list<Opportunity>();
          
           for(Opportunity opp:oppListUpdate){
             opp.StageName= 'Closed Won';
             oppListUpdateExit.add(opp);
          }
          
          update oppListUpdateExit;
          
          
          
    }
}