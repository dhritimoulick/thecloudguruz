/*****************************************************************************
@Name:  config_Cltr     
@=========================================================================
@Purpose: Controller for ProductivityConguration                                                                                              
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR      DATE                DETAIL                                 
@1.0 - Amritesh      12-02-2016         INITIAL DEVELOPMENT              
******************************************************************************/ 
global class Config_Clr {
    
    Public List<String> sObjectList                     {get; set;}
    Public List<BusinessHours> businessHours            {get; set;}
    
    global Config_Clr(){
        
        businessHours       = new List<BusinessHours>();
        sObjectList         = new List<String>();
        
        try{
            businessHours       = ProductivityUtil.getBusinessHours();        
            sObjectList         = ProductivityUtil.getPicklistValues(ProductivityUtil.CONFIG_OBJ, ProductivityUtil.CONFIG_SOBJECT_FIELD);            
                    
        }catch(Exception ex){}
    }
    
    @RemoteAction 
    global static List<sObjectWrapper> getSobjectFields(string sObjName){
        
        List<sObjectWrapper> sObjectList    = new List<sObjectWrapper>();
        try{
            Map<string, string> fieldMap    = ProductivityUtil.getFields(sObjName);
            for(string s : fieldMap.keySet()){
                sObjectList.add(new sObjectWrapper(s,fieldMap.get(s)));
            } 
        }catch(Exception ex){}
        
        return sObjectList;
    }
    
    @RemoteAction
    global static string saveConfig(EPTE__Productivity_Configuration__c parent, List<EPTE__Productivity_Field_Configuration__c> childConfig){
        
        Savepoint sp = Database.setSavepoint();
        boolean isSuccess = false;
        try{
            if(parent.EPTE__Sobject_Type__c != null){ 
                          
                insert parent;
            }
            
            List<EPTE__Productivity_Field_Configuration__c> fieldList    = new List<EPTE__Productivity_Field_Configuration__c>();
            for(EPTE__Productivity_Field_Configuration__c f : childConfig){
                if(f.EPTE__Based_On_Owner_Assignment__c == true || f.EPTE__FieldAPIName__c != null){
                    f.EPTE__Productivity_Configuration__c    = parent.id;
                    fieldList.add(f);
                }
                
            }
            
            insert fieldList;
            isSuccess = true;
            return parent.id;
            
        }catch(Exception ex){
            isSuccess = false;
            Database.rollback(sp); 
            return ex.getMessage();          
        }
        
    }
        
    @RemoteAction
    global static List<fieldWrapper> getFieldValues(string fieldName, string sObjName){
        
        List<fieldWrapper> fieldValList = new List<fieldWrapper>();
        
        try{
            string sobjType = Schema.getGlobalDescribe().get(sObjName).getDescribe().fields.getMap().get(fieldName).getDescribe().getType().name();
            
            if(sobjType == ProductivityUtil.PICKLIST_DATATYPE ){
                fieldValList.add(new fieldWrapper(fieldName,sobjType,ProductivityUtil.getPicklistValues(sObjName, fieldName)));
            }
            
            else if(sobjType == ProductivityUtil.BOOLEAN_DATATYPE ){
                fieldValList.add(new fieldWrapper(fieldName,sobjType,new List<String>{ProductivityUtil.BOOLEAN_TRUE,ProductivityUtil.BOOLEAN_FALSE}));
            }else{
                fieldValList.add(new fieldWrapper(fieldName,sobjType,new List<String>()));
            }
            
        }catch(Exception ex){}
       
        return fieldValList;
    }
    
    global class sObjectWrapper{
        public string fieldName     {get; set;}
        public string fieldLabel    {get; set;}
        
        global sObjectWrapper(string fieldName, string fieldLabel){
            this.fieldName  = fieldName;
            this.fieldLabel = fieldLabel;
        }
    }
    
    global class fieldWrapper {
        public string fieldAPI          {get; set;}
        public string fieldType         {get; set;}
        public List<String> fieldVal    {get; set;}
        
        global fieldWrapper(string fieldAPI, string fieldType, List<string> fieldVal){
            this.fieldAPI   = fieldAPI;
            this.fieldType  = fieldType;
            this.fieldVal   = fieldVal;
        }
    }
}