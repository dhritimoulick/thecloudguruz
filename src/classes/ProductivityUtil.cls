/*****************************************************************************
@Name:  ProductivityUtil     
@=========================================================================
@Purpose: util Class                                                                                             
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
******************************************************************************/ 
Global class ProductivityUtil {
    
    public static final string CONFIG_OBJ				= System.label.Config_Object;
    public static final string CONFIG_SOBJECT_FIELD		= System.label.Config_sObject_Field_API;
    public static final Set<String> fieldTypeSet		= new Set<String>{'BOOLEAN','PICKLIST','STRING','NUMBER'};
    public static final string PICKLIST_DATATYPE		= System.label.PICKLIST_DATATYPE;
    public static final string BOOLEAN_DATATYPE			= System.label.BOOLEAN_DATATYPE;
    public static final string BOOLEAN_TRUE				= System.label.BOOLEAN_TRUE;
    public static final string BOOLEAN_FALSE			= System.label.BOOLEAN_FALSE;
        
    global static String fetchObjectname(Sobject sobj){
        return sobj.getSObjectType().getDescribe().getName();
    }
    
    
    /*
    * @Purpose: Get Business Hour defined in the system 
    * @returnType: List<BusinessHours>
    */
    global static List<BusinessHours> getBusinessHours(){
        return [Select id,Name from BusinessHours where IsActive = true limit 50];
    }
    
    /*
    * @Purpose: Get Picklist field values of any object  
    * @Param : @ObjectApi_name is sObject API Name and @Field_name is the field API Name for 
    *   which picklist values needed
    * @returnType: List<String>
    */
    global static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
        
        List<String> lstPickvals                        = new List<String>();
        Schema.SObjectType targetType                   = Schema.getGlobalDescribe().get(ObjectApi_name);
        Sobject Object_name                             = targetType.newSObject();
        Schema.sObjectType sobject_type                 = Object_name.getSObjectType(); 
        Schema.DescribeSObjectResult sobject_describe   = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map      = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values     = field_map.get(Field_name).getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry a : pick_list_values) {
            lstPickvals.add(a.getValue());
        }
        
        return lstPickvals;
    }
    
    /*
    * @Purpose: Get SObject field API Name and Label
    * @Param : @sObjName
    * @returnType: List<String>
    */
    global static Map<String,String> getFields(string sObjName){
        
        Map<String,String> sObjFieldMap					= new Map<String,String>();
        Map<String,Schema.SObjectType> gd 				= Schema.getGlobalDescribe(); 
        Schema.SObjectType sobjType 					= gd.get(sObjName); 
        Schema.DescribeSObjectResult describeResult 	= sobjType.getDescribe(); 
        Map<String,Schema.SObjectField> fieldsMap 		= describeResult.fields.getMap();
        system.debug(fieldsMap.keySet());
        
        for(String s : fieldsMap.keySet()){
            if(fieldsMap.get(s).getDescribe().isAccessible() && fieldsMap.get(s).getDescribe().isUpdateable()
               && fieldTypeSet.contains(fieldsMap.get(s).getDescribe().getType().name())){
                sObjFieldMap.put(fieldsMap.get(s).getDescribe().getName(),fieldsMap.get(s).getDescribe().getLabel());
            }            
        }     
        return sObjFieldMap;
    }
}