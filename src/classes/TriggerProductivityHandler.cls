/*****************************************************************************
@Name:  TriggerProductivityBusinessLogic     
@=========================================================================
@Purpose: Helper class for productivity   (Generic Class)                                                                                           
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
 ******************************************************************************/ 
Global class TriggerProductivityHandler {
    global TriggerProductivityHandler(){
    }
    
    global static void onbeforeInsert(List<Sobject> onBeforeInsertList,List<Sobject> onAfterInsertList,Map<ID,Sobject> onBeforeUpdateMap,Map<ID,Sobject> onAfterUpdateMap){
        
    }
    
    global static void onAfterInsert(List<Sobject> onBeforeInsertList,List<Sobject> onAfterInsertList,Map<ID,Sobject> onBeforeUpdateMap,Map<ID,Sobject> onAfterUpdateMap){
        evaluateProductivity(onBeforeInsertList,onAfterUpdateMap);
       
    }
    
    global static void onBeforeUpdate(List<Sobject> onBeforeInsertList,List<Sobject> onAfterInsertList,Map<ID,Sobject> onBeforeUpdateMap,Map<ID,Sobject> onAfterUpdateMap){
        
    }
    /*****************************************************************************
	@Name:  onAfterUpdate     
	@=========================================================================
	@Purpose: Helper method to evaluate  productivity of Users,fetch objects dynamically and evaluate productivity                                                                                      
	@=========================================================================
	@History                                                            
	@---------                                                            
	@VERSION AUTHOR                            DATE                DETAIL                                 
	@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
    ******************************************************************************/
    global static void onAfterUpdate(List<Sobject> onBeforeInsertList,List<Sobject> onAfterInsertList,Map<ID,Sobject> onBeforeUpdateMap,Map<ID,Sobject> onAfterUpdateMap){
        Map<Id,Sobject> sobjectList = new Map<Id,Sobject>();
        List<Sobject> exitProductivity = new List<Sobject>();
        String fetchSobjectname;
        Set<String> fetchobjectNamelst = new Set<String>();
        for(Sobject sobj:onAfterUpdateMap.values()){
        	fetchSobjectname = ProductivityUtil.fetchObjectname(sobj);
        	if(sobj.get('OwnerID') <> onBeforeUpdateMap.get(sobj.ID).get('OwnerID')){  //If owner changes
        		    sobjectList.put(sobj.id,sobj);
        		    fetchobjectNamelst.add(fetchSobjectname);
        	}else if(sobj.get('OwnerID') == onBeforeUpdateMap.get(sobj.ID).get('OwnerID')){
        		    exitProductivity.add(sobj);
        	}
        }
        if(sobjectList <> null && sobjectList.size()>0){
        	TriggerProductivityInterface insertRecord = new TriggerProductivityBusinessLogic();
        	List<EPTE__Productivity_Configuration__c> fetchProductivityCobfig = new List<EPTE__Productivity_Configuration__c>(ProductivityDAL.fetchProductivityConfig1(fetchobjectNamelst));
            Set<String> fetchString = new Set<String>();
            for(EPTE__Productivity_Configuration__c fetchs :fetchProductivityCobfig){
            	fetchString.add(fetchs.EPTE__Sobject_Type__c);
            } 
            insertRecord.createNUpdateProductivity(sobjectList,fetchString);    
        }
        if(exitProductivity.size() > 0 && exitProductivity <> null){
        	evaluateProductivityOnUpdate(exitProductivity,onBeforeUpdateMap);
        }
    } 
    /*****************************************************************************
	@Name:  evaluateProductivity     
	@=========================================================================
	@Purpose: Calling after insert,Helper method to fetch Configuration object and return the entry criteria and exit criteria matches the configurable object                                                                                    
	@=========================================================================
	@History                                                            
	@---------                                                            
	@VERSION AUTHOR                            DATE                DETAIL                                 
	@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
    ******************************************************************************/
    global static void evaluateProductivity(List<Sobject> InserRecord,Map<ID,Sobject> onBeforeUpdateMap){
    	String fetchSobjectname;
        Set<String> fetchobjectNamelst = new Set<String>();
        Map<String,Sobject> fetchSobjectDetailsMap = new Map<String,Sobject>();
        for(sobject sobj:InserRecord){
        	fetchSobjectname = ProductivityUtil.fetchObjectname(sobj);
        	fetchobjectNamelst.add(fetchSobjectname);
        	fetchSobjectDetailsMap.put(sobj.Id,sobj);
        }
        
        List<EPTE__Productivity_Configuration__c> fetchProductivityCobfig = new List<EPTE__Productivity_Configuration__c>();
        fetchProductivityCobfig = ProductivityDAL.fetchProductivityConfig(fetchobjectNamelst);
        
        Map<String,List<EPTE__Productivity_Field_Configuration__c>> fetchFieldsLst = new map<String,List<EPTE__Productivity_Field_Configuration__c>>();
        
        for(EPTE__Productivity_Configuration__c fetchFieldSets:fetchProductivityCobfig){ //fetching config object
        	if(fetchFieldSets.Productivity_Field_Configurations__r <> null){
        		fetchFieldsLst.put(fetchFieldSets.EPTE__Sobject_Type__c,fetchFieldSets.Productivity_Field_Configurations__r);
        	}
        }

        Map<ID,Sobject> insertProductivityRecord = new Map<ID,Sobject>();
        Map<ID,Sobject> insertProductivityRecordExit = new Map<ID,Sobject>();
        if(fetchFieldsLst <> null && fetchFieldsLst.size()>0){
        	
	        for(Sobject s:fetchSobjectDetailsMap.values()){
	        	fetchSobjectname = ProductivityUtil.fetchObjectname(s);
	        	String sobjectMergeEntry;
	        	String configmergeEntry;
	        	String sobjectMergeExit;
	        	String configmergeExit;
	        	if(fetchFieldsLst.keyset().contains(fetchSobjectname)){
	        		List<EPTE__Productivity_Field_Configuration__c> fieldValues = new List<EPTE__Productivity_Field_Configuration__c>();
	        		fieldValues = fetchFieldsLst.get(fetchSobjectname);
	        		
	        		for(EPTE__Productivity_Field_Configuration__c fields :fieldValues ){
	        			 String fetchAPIName=fields.EPTE__FieldAPIName__c;
	        			 
	        			 if(fields.EPTE__Rule_Type__c =='Entry' && fields.EPTE__Based_On_Owner_Assignment__c==false){
	        			 	 configmergeEntry = configmergeEntry+' '+fields.EPTE__Value__c;
	        			 	 
	        			 	if(s.get(fetchAPIName)==fields.EPTE__Value__c){
	        			 	  sobjectMergeEntry = sobjectMergeEntry+' '+s.get(fetchAPIName);
	        			 	}
	        			 }if(fields.EPTE__Rule_Type__c =='Exit' && fields.EPTE__Based_On_Owner_Assignment__c==false){
	        			 	 configmergeExit = configmergeExit+' '+fields.EPTE__Value__c;
	        			 	if(s.get(fetchAPIName)==fields.EPTE__Value__c){
	        			 	  sobjectMergeExit = sobjectMergeExit+' '+s.get(fetchAPIName);
	        			 	}
	        			 }else if(fields.EPTE__Rule_Type__c =='Entry' && fields.EPTE__Based_On_Owner_Assignment__c==true){
	        			 	  insertProductivityRecord.put(s.Id,s);
	        			 	
	        			 }
	        		}

	        		if(sobjectMergeEntry == configmergeEntry){
	        			insertProductivityRecord.put(s.Id,s); 
	        		}
	        		if(sobjectMergeExit == configmergeExit){
	        			insertProductivityRecordExit.put(s.Id,s); 
	        		}
	        	}
	        	
	        }
	     }

        if(insertProductivityRecord <> null && insertProductivityRecord.size()>0){
	        TriggerProductivityInterface insertRecord = new TriggerProductivityBusinessLogic(); 
	        insertRecord.createProductivity(insertProductivityRecord,fetchFieldsLst.keyset());    
        }
        if(insertProductivityRecordExit <> null && insertProductivityRecordExit.size()>0){
	         TriggerProductivityInterface insertRecord1 = new TriggerProductivityBusinessLogic(); 
	        insertRecord1.closeProductivity(insertProductivityRecordExit,fetchFieldsLst.keyset());    
        }
        
    }
     /*****************************************************************************
	@Name:  evaluateProductivityOnUpdate     
	@=========================================================================
	@Purpose: Calling after insert,Helper method to fetch Configuration object and return the entry criteria and exit criteria matches the configurable object                                                                                    
	@=========================================================================
	@History                                                            
	@---------                                                            
	@VERSION AUTHOR                            DATE                DETAIL                                 
	@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
    ******************************************************************************/
    global static void evaluateProductivityOnUpdate(List<Sobject> InserRecord,Map<ID,Sobject> onBeforeUpdateMap){
    	String fetchSobjectname;
        Set<String> fetchobjectNamelst = new Set<String>();
        Map<String,Sobject> fetchSobjectDetailsMap = new Map<String,Sobject>();
        for(sobject sobj:InserRecord){
        	fetchSobjectname = ProductivityUtil.fetchObjectname(sobj);
        	System.debug('^^^^^^^^^^^^^^^^^'+fetchSobjectname);
        	fetchobjectNamelst.add(fetchSobjectname);
        	fetchSobjectDetailsMap.put(sobj.Id,sobj);
        }
        List<EPTE__Productivity_Configuration__c> fetchProductivityCobfig = new List<EPTE__Productivity_Configuration__c>();
        fetchProductivityCobfig = ProductivityDAL.fetchProductivityConfig(fetchobjectNamelst);
        
        Map<String,List<EPTE__Productivity_Field_Configuration__c>> fetchFieldsLst = new map<String,List<EPTE__Productivity_Field_Configuration__c>>();
        
        for(EPTE__Productivity_Configuration__c fetchFieldSets:fetchProductivityCobfig){
        	if(fetchFieldSets.Productivity_Field_Configurations__r <> null){
        		fetchFieldsLst.put(fetchFieldSets.EPTE__Sobject_Type__c,fetchFieldSets.Productivity_Field_Configurations__r);
        	}
        }

        Map<ID,Sobject> insertProductivityRecord = new Map<ID,Sobject>();
        Map<ID,Sobject> insertProductivityRecordExit = new Map<ID,Sobject>();
        Boolean check;
        Boolean checkExit;
        List<EPTE__Productivity__c> sobjectProductivity = [Select ID from EPTE__Productivity__c where EPTE__sObject_Record_Id__c in :fetchSobjectDetailsMap.keyset()];
        List<EPTE__Productivity__c> sobjectProductivityExit = [Select ID from EPTE__Productivity__c where EPTE__sObject_Record_Id__c in :fetchSobjectDetailsMap.keyset() AND EPTE__Start_Time__c <> null AND EPTE__End_Time__c <> null];
         if(sobjectProductivity.size()>=1){
        	checkExit = true;
        }
        if(sobjectProductivity.size()==0){
        	check = true;
        }
        if(fetchFieldsLst <> null && fetchFieldsLst.size()>0){ 
        	
	        for(Sobject s:fetchSobjectDetailsMap.values()){
	        	fetchSobjectname = ProductivityUtil.fetchObjectname(s);
	        	String sobjectMergeEntry;
	        	String configmergeEntry;
	        	String sobjectMergeExit;
	        	String configmergeExit;
	        	if(fetchFieldsLst.keyset().contains(fetchSobjectname)){
	        		List<EPTE__Productivity_Field_Configuration__c> fieldValues = new List<EPTE__Productivity_Field_Configuration__c>();
	        		fieldValues = fetchFieldsLst.get(fetchSobjectname);
	        		
	        		for(EPTE__Productivity_Field_Configuration__c fields :fieldValues ){
	        			 String fetchAPIName=fields.EPTE__FieldAPIName__c;
	        			
	        			 if(fields.EPTE__Rule_Type__c =='Entry' && fields.EPTE__Based_On_Owner_Assignment__c==false){
	        			 	 configmergeEntry = configmergeEntry+' '+fields.EPTE__Value__c;
	        			 	 
	        			 	if((s.get(fetchAPIName)==fields.EPTE__Value__c)){
	        			 	  sobjectMergeEntry = sobjectMergeEntry+' '+s.get(fetchAPIName);
	        			 	}
	        			 }if(fields.EPTE__Rule_Type__c =='Exit' && fields.EPTE__Based_On_Owner_Assignment__c==false){

	        			 	 configmergeExit = configmergeExit+' '+fields.EPTE__Value__c;
	        			 	if(s.get(fetchAPIName)==fields.EPTE__Value__c){
	        			 	  sobjectMergeExit = sobjectMergeExit+' '+s.get(fetchAPIName);
	        			 	}
	        			 }
	        		}

	        		if((sobjectMergeEntry == configmergeEntry) && (sobjectMergeEntry <> '' && configmergeEntry <> '') ){
	        			if(check == true){
	        			insertProductivityRecord.put(s.Id,s); 
	        			}
	        		}
	        		if((sobjectMergeExit == configmergeExit) && (sobjectMergeEntry <> '' && configmergeEntry <> '')){
	        			if(checkExit == true){
	        				insertProductivityRecordExit.put(s.Id,s);  
	        			}
	        		}
	        	}
	        	
	        }
	         System.debug('^^^^^^^^^^^^^^^insertProductivityRecord^^^^^^^^^^^'+insertProductivityRecord);
	         System.debug('^^^^^^^^^^^^^^^insertProductivityRecordExit^^^^^^^^^^^'+insertProductivityRecordExit);
	        
        }

        if(insertProductivityRecord <> null && insertProductivityRecord.size()>0){
	         TriggerProductivityInterface insertRecord = new TriggerProductivityBusinessLogic();
	        insertRecord.createProductivity(insertProductivityRecord,fetchFieldsLst.keyset());    
        }
        if(insertProductivityRecordExit <> null && insertProductivityRecordExit.size()>0){
	         TriggerProductivityInterface insertRecord1 = new TriggerProductivityBusinessLogic();
	         insertRecord1.closeProductivity(insertProductivityRecordExit,fetchFieldsLst.keyset());    
        }
    }
    
}