/*****************************************************************************
@Name:  UtilityTest     
@Purpose:  Created dummy data for Lead,Account,Opportunity,Task,Case object.                                                                                                              
******************************************************************************/ 
global class UtilityTest {

    /********************************************
    @Name :getdummyAccount
    @Return type : Account
    ********************************************/ 
	 global static Account getdummyAccount(Integer i)
	
	  {
	     Account accInsert = new Account();
	     accInsert.Name='CloudGuruz'+i;
	     accInsert.Type= 'Prospect';
	     return accInsert;
	  }
    /********************************************
    @Name :getdummyLead
    @Return type : Lead
    ********************************************/
	 global static Lead getdummyLead(Integer i){
	
	        Lead l = new Lead();
	        l.LastName='CloudGuru'+i;
	        l.Company='ET Marlabs';
	        l.Status='Closed - Not Converted';
	        return l;
	 }
    /********************************************
    @Name :getdummyOpportunity
    @Return type : Opportunity
    ********************************************/
    global static Opportunity getdummyOpportunity(Integer i){
        Opportunity Op = new Opportunity();
        Op.Name='CloudGuruz'+i;
        Op.CloseDate=Date.today();
        Op.StageName='Assigned';
        return op;
    }
    /********************************************
    @Name :getdummyTask
    @Return type : Task
    ********************************************/
    global static Task getdummyTask(Integer i){
        Task ts = new Task();
        ts.Status='In Progress';
        ts.Subject='Call';
        ts.Priority='High';
        return ts;
    }
    
    /********************************************
    @Name :getdummyCase
    @Return type : Case
    ********************************************/
    global static Case getdummyCase(Integer i){
        Case cs = new Case();
        cs.Status='Assigned';
        cs.Origin='Phone';
        return cs;
    }   
     /********************************************
    @Name :getdummyCase
    @Return type : Case
    ********************************************/
   global static User getdummyUser(){
        Profile profileNames = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userNames = new User(Alias = 'Cloudg', Email='Cloudguruz@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Cloudguruz', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = profileNames.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='Cloudguruz@testorg.com');
        return usernames;
   }    
}