/*****************************************************************************
@Name:  ProductivityDAL     
@=========================================================================
@Purpose: All Queries related to productivity will be stored in this class                                                                                             
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
 ******************************************************************************/ 
global class ProductivityDAL {
	   /*****************************************************************************
		@Name:  fetchProductivityConfig     
		@=========================================================================
		@Purpose: Fetching config object and config field setting object                                                                                            
		@=========================================================================
		@History                                                            
		@---------                                                            
		@VERSION AUTHOR                            DATE                DETAIL                                 
		@1.0 - Dhriti Krishna Ghosh Moulick      12-02-2016         INITIAL DEVELOPMENT              
		******************************************************************************/ 
       global static List<EPTE__Productivity_Configuration__c> fetchProductivityConfig(Set<String> fetchobjectNamelst){
       	    return [Select Id,Name,EPTE__Business_Hours__c,EPTE__Sobject_Type__c,(Select Id,EPTE__Based_On_Owner_Assignment__c,EPTE__FieldAPIName__c,EPTE__Field_Name__c,EPTE__Productivity_Configuration__c,EPTE__Rule_Type__c,EPTE__Value__c
       	     from Productivity_Field_Configurations__r) from EPTE__Productivity_Configuration__c where EPTE__Sobject_Type__c in:fetchobjectNamelst];
       }
       
       global static List<EPTE__Productivity__c> fetchToCloseDate(Set<ID> insertProductivitylst, List<EPTE__Productivity__c> insertProductivitylsts){
       	    return [Select ID,EPTE__End_Time__c,EPTE__Business_Hours__c,EPTE__Start_Time__c from EPTE__Productivity__c where EPTE__End_Time__c =:null AND EPTE__sObject_Record_Id__c in:insertProductivitylst AND (NOT ID in:insertProductivitylsts)];
       }
       
       global static List<EPTE__Productivity__c> fetchToCloseProductivity(Set<ID> insertProductivitylst){
       	    return [Select ID,EPTE__End_Time__c,EPTE__Business_Hours__c,EPTE__Start_Time__c from EPTE__Productivity__c where EPTE__End_Time__c =:null AND EPTE__sObject_Record_Id__c in:insertProductivitylst];
       }
        
        global static list<EPTE__Productivity_Configuration__c> fetchProductivityConfig1(Set<String> fetchobjectNamelst){
       	    return [Select Id,Name,EPTE__Business_Hours__c,EPTE__Sobject_Type__c from EPTE__Productivity_Configuration__c where EPTE__Sobject_Type__c in:fetchobjectNamelst];
       }  
       
       global static BusinessHours fetchBusinessHours(){
       	   return [SELECT Id,IsDefault FROM BusinessHours where IsDefault = true];
       }
       
}