/*****************************************************************************
@Name:  Test_Config_Clr    
@=========================================================================
@Purpose: Test Class for Config_Clr Controller                                                                                             
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR      DATE                DETAIL                                 
@1.0 - Amritesh      12-03-2016         INITIAL DEVELOPMENT              
******************************************************************************/
@isTest 
global class Test_Config_Clr {
    
    static testmethod void testConfig(){
    
        User dummyUser    = UtilityTest.getdummyUser();
        insert dummyUser;
        
        system.runAs(dummyUser){
            
            Config_Clr obj            = new Config_Clr();
            Config_Clr.getSobjectFields('Case');
            Config_Clr.getFieldValues('Status','Case');  
            
           
            
            EPTE__Productivity_Configuration__c master    = new EPTE__Productivity_Configuration__c();
            master.EPTE__Sobject_Type__c                  = 'Lead';
            
            List<EPTE__Productivity_Field_Configuration__c> dummyfieldList = new List<EPTE__Productivity_Field_Configuration__c>();
            for(integer i=0;i<5;i++){
                dummyfieldList.add(new EPTE__Productivity_Field_Configuration__c(EPTE__FieldAPIName__c='LastName',EPTE__Rule_Type__c='Entry',EPTE__Value__c='DummyName'+i));
            }
            
            Test.StartTest();
            Config_Clr.saveConfig(master, dummyfieldList);
            
            Test.StopTest();
        }
    
   }
}